package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.stream.Collectors;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.isEmpty() || inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        int rows = 0, len = inputNumbers.size(), correctLen;
        for (correctLen = 0; correctLen < len; correctLen += rows) {
            rows++;
        }
        if (correctLen != len) {
            throw new CannotBuildPyramidException();
        }
        int columns = 2 * rows - 1;

        if (rows <= 0 || columns <= 0) {
            throw new CannotBuildPyramidException();
        }
        int[][] result = new int[rows][columns];
        inputNumbers = inputNumbers.stream().sorted().collect(Collectors.toList());

        int mid = columns / 2, pointer = 0, col;
        for (int i = 0; i < rows; i++) {
            col = mid - i;
            for (int j = 0; j < i + 1; j++) {
                result[i][col] = inputNumbers.get(pointer++);
                col += 2;
            }
        }

        return result;
    }


}
