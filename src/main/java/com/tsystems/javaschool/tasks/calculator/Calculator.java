package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayDeque;
import java.util.Deque;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty() || checkBrackets(statement)) {
            return null;
        }
        return change(getAnswer(statement));
    }

    private String change(String answer) {
        try {
            int dotIndex = answer.indexOf('.');
            if (answer.substring(dotIndex).equals(".0")) {
                return answer.substring(0, dotIndex);
            }
            if (answer.length() - dotIndex > 5) {
                if (Integer.parseInt(String.valueOf(answer.charAt(dotIndex + 5))) > 4) {
                    answer = answer.replace(answer.charAt(dotIndex + 4), (char) (answer.charAt(dotIndex + 4) + 1));
                }
                answer = answer.substring(0, dotIndex + 5);
            }
            return answer;
        } catch (Exception e) {
            return null;
        }
    }

    private boolean checkBrackets(String statement) {
        Deque<Character> stack = new ArrayDeque<>();
        for (char c : statement.toCharArray()) {
            if (c == '(') {
                stack.push(c);
            } else if (c == ')') {
                if (stack.isEmpty() || stack.pop() != '(') {
                    return true;
                }
            }
        }
        return !stack.isEmpty();
    }

    private String getAnswer(String statement) {
        String rpn = ExpressionToRPN(fixExpression(statement));
        StringBuilder operand = new StringBuilder();
        Deque<String> symbols = new ArrayDeque<>();

        for (int i = 0; i < rpn.length(); i++) {
            if (rpn.charAt(i) == ' ') continue;
            if (symbolPriority(rpn.charAt(i)) == 0) {
                while (rpn.charAt(i) != ' ' && symbolPriority(rpn.charAt(i)) == 0) {
                    operand.append(rpn.charAt(i++));
                    if (i == rpn.length()) break;
                }
                symbols.push(String.valueOf(operand));
                operand.setLength(0);
            }

            if (symbolPriority(rpn.charAt(i)) > 1) {
                double a, b;
                try {
                    a = Double.parseDouble(symbols.pop());
                    b = Double.parseDouble(symbols.pop());
                } catch (Exception e) {
                    return null;
                }
                if (rpn.charAt(i) == '+') symbols.push(String.valueOf(b + a));
                if (rpn.charAt(i) == '-') symbols.push(String.valueOf(b - a));
                if (rpn.charAt(i) == '*') symbols.push(String.valueOf(b * a));
                if (rpn.charAt(i) == '/') symbols.push(String.valueOf(b / a));
            }
        }

        return symbols.pop();
    }

    private String ExpressionToRPN(String statement) {
        StringBuilder current = new StringBuilder();
        Deque<Character> symbols = new ArrayDeque<>();
        int priority;

        for (int i = 0; i < statement.length(); i++) {
            priority = symbolPriority(statement.charAt(i));

            if (priority == 0) current.append(statement.charAt(i));
            if (priority == 1) symbols.push(statement.charAt(i));

            if (priority > 1) {
                current.append(' ');

                while (!symbols.isEmpty()) {
                    if (symbolPriority(symbols.peek()) >= priority) {
                        current.append(symbols.pop());
                    } else break;
                }
                symbols.push(statement.charAt(i));
            }

            if (priority == -1) {
                current.append(' ');

                while (symbolPriority(symbols.peek()) != 1) {
                    current.append(symbols.pop());
                }
                symbols.pop();
            }
        }

        while (!symbols.isEmpty()) {
            current.append(symbols.pop());
        }

        return current.toString();
    }

    private String fixExpression(String statement) {
        StringBuilder fixedExpr = new StringBuilder();
        for (int i = 0; i < statement.length(); i++) {
            if (statement.charAt(i) == '-') {
                if (i == 0 || statement.charAt(i - 1) == '(') {
                    fixedExpr.append('0');
                }
            }
            fixedExpr.append(statement.charAt(i));
        }
        return fixedExpr.toString();
    }

    private int symbolPriority(char symbol) {
        if (symbol == '*' || symbol == '/') return 3;
        else if (symbol == '+' || symbol == '-') return 2;
        else if (symbol == '(') return 1;
        else if (symbol == ')') return -1;
        else return 0;
    }
}